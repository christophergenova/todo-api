// call mongoose since we are creating a model
const mongoose = require('mongoose');

// to create a mongoose schema, we need to access the schema class in mongoose

const Schema = mongoose.Schema;

// Next, create the category schema
// In a schema, id is automatically created, therefore, we don't need to include it
const CategorySchema = new Schema({
    name: String,

});

// Export the schema so other files can access it
module.exports = mongoose.model('Category', CategorySchema);
