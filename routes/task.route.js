// We will define the routes, at the same time the process; like: Route&Controller

// Since we are setting up the routes , we need to call express
const express = require("express");

// we need to instantiate Router() or the method that will help us set up the routes.

const taskRouter = express.Router();

// call the model file that we will access/modify
const Task = require("../models/task.model");
const { replaceOne } = require("../models/task.model");
// in JS, you dont need to include the extension. no ".js"

// GET ALL TASKS
taskRouter.get("/tasks", async (req, res) => {
  // In mongoose--- Mode.method() .find() -- will return all items.
  // await will use if it is inside "async"
  // const tasks = await Task.find();
  const tasks = await Task.find().populate(["categoryId", "assignee"]);
  // send a response to the origin of request
  // this is important
  res.json(tasks);
});

// ADD TASKS
taskRouter.post("/add-task", async (req, res) => {
  //assuming, the data came from a form
  // we can access the form-data in JSON via req.body.inputName
  const title = req.body.title;
  const body = req.body.body;
  const isDone = req.body.isDone;
  const categoryId = req.body.categoryId;
  const assignee = req.body.assignee;
  // insert data in our db
  const task = await Task.create({
    title: title,
    body: body,
    isDone: isDone,
    categoryId: categoryId,
    assignee: assignee,
  });
  res.json(task);
});

// GET TASKS BY ID
taskRouter.get("/tasks/:id", async (req, res) => {
  // so we can access the data from the url by using req.params.dataName
  const id = req.params.id;
  const task = await Task.findById(id);
  // we need to always put a response
  res.json(task);
});

// update a Task by ID
taskRouter.patch("/update-task/:id", async (req, res) => {
  // when updating we need the following
  // 1st the ID of the item we want to update
  const id = req.params.id;
  // 2nd we need to create an updated object, containing the field we want to update and its value;
  const update = {
    title: req.body.title,
    body: req.body.body,
    isDone: req.body.isDone,
    categoryId: req.body.categoryId,
    assignee: req.body.assignee,
  };

  const task = await Task.findByIdAndUpdate(id, update, { new: true });
  res.json(task);
});

taskRouter.delete("/delete-task/:id", async (req, res) => {
  const id = req.params.id;
  const task = await Task.findByIdAndDelete(id);
  res.json(task);
});

module.exports = taskRouter;
