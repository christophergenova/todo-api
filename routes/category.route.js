// We will define the routes, at the same time the process; like: Route&Controller

// Since we are setting up the routes , we need to call express
const express = require('express');

// we need to instantiate Router() or the method that will help us set up the routes.

const categoryRouter = express.Router();

// call the model file that we will access/modify
const Category = require('../models/category.model');
// in JS, you dont need to include the extension. no ".js"

// Get all categories
// req- incoming data
// res-> the response we send to the requestor
categoryRouter.get('/categories', async(req, res)=>{
    // In mongoose--- Mode.method() .find() -- will return all items.
    // await will use if it is inside "async"
    const categories = await Category.find();
    // send a response to the origin of request
    // this is important
    res.json(categories);

});

// add category
categoryRouter.post('/categories', async(req, res)=>{
    //assuming, the data came from a form
    // we can access the form-data in JSON via req.body.inputName
    const name = req.body.name;
    // insert data in our db
    const category = await Category.create({
        name: name
    });
    res.json(category);

});

// show category by Id
categoryRouter.get('/categories/:id', async(req, res)=>{
    // so we can access the data from the url by using req.params.dataName
    const id = req.params.id;
    const category = await Category.findById(id);
    // we need to always put a response
    res.json(category);
});

// Delete a category
categoryRouter.delete('/categories/:id', async(req, res)=>{
    const id = req.params.id;
    const category = await Category.findByIdAndDelete(id);
    res.json(category);
});

// update a category
categoryRouter.patch('/categories/:id', async(req, res)=>{
    // when updating we need the following
    // 1st the ID of the item we want to update
    const id = req.params.id;
    // 2nd we need to create an updated object, containing the field we want to update and its value;
    const update = {
        name: req.body.name
    }

    const category = await Category.findByIdAndUpdate(id, update, {new:true});
    res.json(category);

});

// export the router
module.exports = categoryRouter;

