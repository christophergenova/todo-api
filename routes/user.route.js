const User = require("../models/user.model");

const express = require("express");
const UserRouter = express.Router();
const bcrypt = require("bcryptjs");

// get all users
// Using the CategoryRouter as a format, create the route and function for get all users;

UserRouter.get("/users", async (req, res) => {
  // In mongoose--- Mode.method() .find() -- will return all items.
  // await will use if it is inside "async"
  const users = await User.find();
  // send a response to the origin of request
  // this is important
  res.json(users);
});

// add UserRouter
UserRouter.post("/register", async (req, res) => {
  //assuming, the data came from a form
  // we can access the form-data in JSON via req.body.inputName
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const email = req.body.email;
  const password = req.body.password;

  // find the user using the given ID, if the query returns something, then the email is not unique
  const checkUser = await User.find({
    email: email,
  });

  if (checkUser.length > 0) {
    res.json("Email Exists");
  } else {
    const salt = bcrypt.genSaltSync(10);
    const hashedPassword = bcrypt.hashSync(password, salt);

    // insert data in our db
    const users = await User.create({
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: hashedPassword,
    });
    res.json(users);
  }
});

// LOGIN
UserRouter.post("/login", async (req, res) => {
  const { email, password } = req.body;
  // find the user using the email provided
  const user = await User.findOne({ email: email });

  if (!user) res.json("Wrong Email");

  // decode and compare the password using bcrypt
  const matchedPassword = await bcrypt.compare(password, user.password);

  if (!matchedPassword) res.json("wrong password");

  res.json(user);
});

module.exports = UserRouter;
