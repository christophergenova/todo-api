const express = require("express");

// call mongoose for db connection
const mongoose = require("mongoose");

// create an instance of express;
const app = express();

const PORT = 4000;

// Parse the incoming data
app.use(express.json()); // Parse json data
app.use(express.urlencoded({ extended: false })); // Parse data from a url

// Allow CORS
const cors = require("cors");
app.use(cors());

// DB connection string from MongoDB
const dbURL =
  "mongodb+srv://admintops:toperpassword@b67cluster.hr1th.gcp.mongodb.net/b67cluster?retryWrites=true&w=majority";

mongoose
  .connect(dbURL, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.log("Remote Database Connection Successful");
  });

// to use the routes we created, first import them
const categoryRouter = require("./routes/category.route");
const userRouter = require("./routes/user.route");
const taskRouter = require("./routes/task.route");

// register the route
app.use("/api", categoryRouter);
app.use("/api", userRouter);
app.use("/api", taskRouter);

app.listen(PORT, () => {
  console.log("Listening on Port: " + PORT);
}); // will check the app if running

// Todo App
//tasks
// - taskName
// - taskBody
// - assignee
// - category

//categories
// - name

//users
// - firstName
// - lastName
// - email
// - password
